import { useState } from "react";
import useInterval from "./useInterval.js";

const App = () => {
  let [count, setCount] = useState(0);
  let [delay, setDelay] = useState(null);
  useInterval(() => {
    setCount(count + 1);
  }, delay);

  return (
    <div className="App">
      <p>{count}</p>
      <button onClick={() => setDelay(1000)}>start</button>
      <button onClick={() => setDelay(null)}>stop</button>
    </div>
  );
};

export default App;
